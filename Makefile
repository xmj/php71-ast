# Created by: Johannes Meixner <johannes@perceivon.net>
# $FreeBSD$

PORTNAME=	php-ast
PORTVERSION=	0.1.4
DISTVERSIONPREFIX=	v
CATEGORIES=	devel

MAINTAINER=	johannes@perceivon.net
COMMENT=	Extension exposing PHP 7 abstract syntax tree

LICENSE=	BSD3CLAUSE
LICENSE_FILE=	${WRKSRC}/LICENSE

USE_GITHUB=	yes
GH_ACCOUNT=	nikic

USES=		php:phpize
PLIST_SUB=	PHP_EXTDIR="${PHP_EXT_DIR}"

do-build:
	@(cd ${BUILD_WRKSRC}; ${SETENV} ${MAKE_ENV} ${MAKE} \
		${MAKE_FLAGS} ${MAKEFILE} ${MAKE_ARGS} ${ALL_TARGET})

do-install:
	@${MKDIR} ${STAGEDIR}${PREFIX}/lib/php/${PHP_EXT_DIR}
	${INSTALL_LIB} ${WRKSRC}/modules/ast.so \
		${STAGEDIR}${PREFIX}/lib/php/${PHP_EXT_DIR}
	@${MKDIR} ${STAGEDIR}${PREFIX}/etc/php
	@${ECHO_CMD} "extension=${PREFIX}/lib/php/${PHP_EXT_DIR}/ast.so" > \
		${STAGEDIR}${PREFIX}/etc/php/ext-30-ast.ini

.include <bsd.port.mk>
